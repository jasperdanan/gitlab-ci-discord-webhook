#!/bin/bash

case $1 in
  "shutdown" )
    EMBED_COLOR=16705372
    TITLE="Bot Shutdown"
    DESC="Going to nap for a bit.. brb!"
    ;;

  "restored" )
    EMBED_COLOR=5763719
    TITLE="Bot Restore"
    DESC="Annyeong, I'm back!"
    ;;

  "failure" )
    EMBED_COLOR=15548997
    TITLE="Build Failed"
    DESC="Uh oh, something went wrong.."
    ;;

  * )
    EMBED_COLOR=0
    TITLE="Status Unknown"
    DESC="Uh oh, something went very very wrong.."
    ;;
esac

shift

if [ $# -lt 1 ]; then
  echo -e "WARNING!!\nYou need to pass the WEBHOOK_URL environment variable as the second argument to this script.\nFor details & guide, visit: https://github.com/DiscordHooks/gitlab-ci-discord-webhook" && exit
fi

TIMESTAMP=$(date --utc +%FT%TZ)

WEBHOOK_DATA='{
  "username": "Munji",
  "avatar_url": "https://cdn.discordapp.com/avatars/511315356787408917/afc62bc25f0f3eae9e08afdf80dabfb3.png",
  "embeds": [ {
    "color": '$EMBED_COLOR',
    "title": "'"$TITLE"'",
    "description": "'"$DESC"'",
    "timestamp": "'"$TIMESTAMP"'"
    } ]
  }'

for ARG in "$@"; do
  echo -e "[Webhook]: Sending webhook to Discord...\\n";

  (curl --fail --progress-bar -A "GitLabCI-Webhook" -H Content-Type:application/json -H X-Author:k3rn31p4nic#8383 -d "$WEBHOOK_DATA" "$ARG" \
  && echo -e "\\n[Webhook]: Successfully sent the webhook.") || echo -e "\\n[Webhook]: Unable to send webhook."
done
